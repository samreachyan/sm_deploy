## How to use

Install docker and docker-compose on linux:
```
sudo apt install -y docker-compose && sudo curl -sSL get.docker.com | sh 

sudo systemctl enable docker
```

Clone deploy:

```
git clone https://gitlab.com/samreachyan/sm_deploy && cd sm_deploy
```

Start docker compose
```
docker-compose up -d
```
